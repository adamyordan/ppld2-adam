require 'rbcaw/conduit'

RSpec.describe RbCAW::Conduit, '.project#search' do
  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'user_search', :record => :new_episodes
    end

    it 'should retrieve projects\' info properly' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])
      search_result = conduit.project.search(constraints: {'phids': ['PHID-PROJ-es6tp6oh23oups5huwlc']})

      expect(search_result).to_not eql(nil)
      expect(search_result.results).to_not eql(nil)
      expect(search_result.results.length).to eql(1)

      expect(search_result.results[0]).to respond_to(:phid)
      expect(search_result.results[0]).to respond_to(:id)
      expect(search_result.results[0]).to respond_to(:name)
      expect(search_result.results[0]).to respond_to(:description)
      expect(search_result.results[0]).to respond_to(:icon)
      expect(search_result.results[0]).to respond_to(:color)
      expect(search_result.results[0]).to respond_to(:members)
      expect(search_result.results[0]).to respond_to(:slug)
      expect(search_result.results[0]).to respond_to(:date_created)
      expect(search_result.results[0]).to respond_to(:date_modified)
      expect(search_result.results[0]).to respond_to(:start_date)
      expect(search_result.results[0]).to respond_to(:end_date)
      expect(search_result.results[0]).to respond_to(:is_sprint)

      expect(search_result.results[0].phid).to eql('PHID-PROJ-es6tp6oh23oups5huwlc')
      expect(search_result.results[0].name).to eql('Unit Test Project')
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'while disconnected from the Phabricator host' do
    before do
      stub_request(:any, /#{CONDUIT['host']}/).to_timeout
    end

    it 'should not be able to retrieve info and raised a TimeoutError' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])

      expect { conduit.project.search }.to raise_error(Timeout::Error)
    end
  end
end