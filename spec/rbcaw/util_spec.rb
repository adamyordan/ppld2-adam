require 'rbcaw/conduit'

RSpec.describe RbCAW::Conduit, '#initialize' do
  it 'should initialize properly' do
    conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])

    expect(conduit.host).not_to eql(nil)
  end
end

RSpec.describe RbCAW::Conduit, '.util#ping' do
  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'util_ping', :record => :new_episodes
    end

    it 'should be able to request ping to Phabricator host' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])
      ping_result = conduit.util.ping

      expect(ping_result).not_to eql(nil)
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'while disconnected from the Phabricator host' do
    before do
      stub_request(:any, /#{CONDUIT['host']}/).to_timeout
    end

    it 'should not be able to request ping to Conduit' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])

      expect { conduit.util.ping }.to raise_error(Timeout::Error)
    end
  end
end