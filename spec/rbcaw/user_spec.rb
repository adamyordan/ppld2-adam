require 'rbcaw/conduit'

RSpec.describe RbCAW::Conduit, '.user#whoami' do
  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'user_whoami', :record => :new_episodes
    end

    it 'should retrieve my info properly' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])
      me = conduit.user.whoami

      expect(me).to_not eql(nil)

      expect(me).to respond_to(:phid)
      expect(me).to respond_to(:username)
      expect(me).to respond_to(:username)
      expect(me).to respond_to(:image)
      expect(me).to respond_to(:roles)

      expect(me.phid).to eql('PHID-USER-5fk6iyso3a337hlnwg6x')
      expect(me.username).to eql('unit_test')
      expect(me.realname).to eql('Unit Test Bot')
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'while disconnected from the Phabricator host' do
    before do
      stub_request(:any, /#{CONDUIT['host']}/).to_timeout
    end

    it 'should not be able to retrieve info and raised a TimeoutError' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])

      expect { conduit.user.whoami }.to raise_error(Timeout::Error)
    end
  end
end

RSpec.describe RbCAW::Conduit, '.user#search' do
  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'user_search', :record => :new_episodes
    end

    it 'should retrieve users\' info properly' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])
      search_result = conduit.user.search(constraints: {'phids': ['PHID-USER-5fk6iyso3a337hlnwg6x']})

      expect(search_result).to_not eql(nil)
      expect(search_result.results).to_not eql(nil)
      expect(search_result.results.length).to eql(1)

      expect(search_result.results[0]).to respond_to(:phid)
      expect(search_result.results[0]).to respond_to(:username)
      expect(search_result.results[0]).to respond_to(:username)
      expect(search_result.results[0]).to respond_to(:image)
      expect(search_result.results[0]).to respond_to(:roles)

      expect(search_result.results[0].phid).to eql('PHID-USER-5fk6iyso3a337hlnwg6x')
      expect(search_result.results[0].username).to eql('unit_test')
      expect(search_result.results[0].realname).to eql('Unit Test Bot')
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'while disconnected from the Phabricator host' do
    before do
      stub_request(:any, /#{CONDUIT['host']}/).to_timeout
    end

    it 'should not be able to retrieve info and raised a TimeoutError' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])

      expect { conduit.user.search }.to raise_error(Timeout::Error)
    end
  end
end

RSpec.describe RbCAW::Conduit, '.user#get' do
  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'user_get', :record => :new_episodes
    end

    it 'should retrieve unit_test\'s info properly by PHID' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])
      user = conduit.user.get(phid: 'PHID-USER-5fk6iyso3a337hlnwg6x')

      expect(user).to_not eql(nil)

      expect(user).to respond_to(:phid)
      expect(user).to respond_to(:username)
      expect(user).to respond_to(:username)
      expect(user).to respond_to(:image)
      expect(user).to respond_to(:roles)

      expect(user.phid).to eql('PHID-USER-5fk6iyso3a337hlnwg6x')
      expect(user.username).to eql('unit_test')
      expect(user.realname).to eql('Unit Test Bot')
    end

    it 'should retrieve unit_test\'s info properly by username' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])
      user = conduit.user.get(username: 'unit_test')

      expect(user).to_not eql(nil)

      expect(user).to respond_to(:phid)
      expect(user).to respond_to(:username)
      expect(user).to respond_to(:username)
      expect(user).to respond_to(:image)
      expect(user).to respond_to(:roles)

      expect(user.phid).to eql('PHID-USER-5fk6iyso3a337hlnwg6x')
      expect(user.username).to eql('unit_test')
      expect(user.realname).to eql('Unit Test Bot')
    end

    it 'should fail to retrieve unit_test\'s info when neither PHID nor username is supplied' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])
      user = conduit.user.get

      expect(user).to eql(nil)
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'while disconnected from the Phabricator host' do
    before do
      stub_request(:any, /#{CONDUIT['host']}/).to_timeout
    end

    it 'should not be able to retrieve unit_test\' info and raised a TimeoutError' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])

      expect {
        conduit.user.get(phid: 'PHID-USER-5fk6iyso3a337hlnwg6x')
      }.to raise_error(Timeout::Error)
    end
  end
end