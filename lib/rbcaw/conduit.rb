require 'net/http'
require 'uri'
require 'json'

require_relative './fetchers/maniphest_conduit_fetcher'
require_relative './fetchers/project_conduit_fetcher'
require_relative './fetchers/sprint_conduit_fetcher'
require_relative './fetchers/user_conduit_fetcher'
require_relative './fetchers/util_conduit_fetcher'

module RbCAW
  class Conduit
    attr_reader :host, :maniphest, :project, :sprint, :user, :util

    def initialize(host, api_token)
      @host = host
      @api_token = api_token
      @uri = URI.parse(host)

      @maniphest = ManiphestConduitFetcher.new(self)
      @project = ProjectConduitFetcher.new(self)
      @sprint = SprintConduitFetcher.new(self)
      @user = UserConduitFetcher.new(self)
      @util = UtilConduitFetcher.new(self)
    end

    def request(method, params: {}, session_token: nil, output: 'json')
      http = Net::HTTP.new(@uri.host, @uri.port)
      http.use_ssl = (@uri.scheme == 'https')

      request = Net::HTTP::Post.new("/api/#{method}")

      params['__conduit__'] = {
          'token' => (session_token != nil ? session_token : @api_token)
      }

      form_data = {
          '__conduit__' => '1',
          'output' => output,
          'params' => params.to_json.to_s
      }

      request.set_form_data(form_data)

      response = http.request(request)
      json_response = JSON.parse(response.body)

      if json_response['error_code'] != nil
        raise ArgumentError, "#{json_response['error_code']}: #{json_response['error_info']}"
      end

      return json_response
    end
  end
end