require_relative '../models/conduit_lazy_model'

module RbCAW
  class SprintInfoLazyModel < ConduitLazyModel
    attr_reader :id
    def initialize(id, conduit: nil)
      @fetched = false

      @id = id
      @conduit = conduit
    end

    def manual_init(point, is_closed)
      @point = point
      @is_closed = is_closed

      @fetched = true
    end

    def fetch
      sprint_info = @conduit.sprint.info(@id)

      manual_init(sprint_info.point, sprint_info.is_closed)
    end

    def point
      check_fetched
      return @point
    end

    def is_closed
      check_fetched
      return @is_closed
    end

    def to_s
      return "#{{id: @id, point: @point, is_closed: @is_closed, fetched: @fetched}}"
    end
  end
end
