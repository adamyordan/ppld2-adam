require_relative '../models/conduit_lazy_model'
require_relative '../models/sprint_info_lazy_model'

module RbCAW
  class TaskLazyModel < ConduitLazyModel
    attr_reader :sprint_info

    def manual_init(id, name, author, owner, status, priority,
                    projects, date_created, date_modified)
      @id = id
      @name = name
      @author = author
      @owner = owner
      @status = status
      @priority = priority
      @projects = projects
      @date_created = date_created
      @date_modified = date_modified

      @sprint_info = SprintInfoLazyModel.new(@id, conduit: @conduit)

      @fetched = true
    end

    def fetch
      task = @conduit.maniphest.get(phid: @phid)

      manual_init(
          task.id, task.name, task.author, task.owner, task.status,
          task.priority, task.projects, task.date_created, task.date_modified
      )
    end

    def id
      check_fetched
      return @id
    end

    def name
      check_fetched
      return @name
    end

    def author
      check_fetched
      return @author
    end

    def owner
      check_fetched
      return @owner
    end

    def status
      check_fetched
      return @status
    end

    def priority
      check_fetched
      return @priority
    end

    def projects
      check_fetched
      return @projects
    end

    def date_created
      check_fetched
      return @date_created
    end

    def date_modified
      check_fetched
      return @date_modified
    end
  end
end
