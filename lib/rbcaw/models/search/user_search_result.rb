require_relative '../../models/search/search_result'
require_relative '../../models/user_lazy_model'

module RbCAW
  class UserSearchResult < SearchResult
    def initialize(conduit, json)
      users_json = json['result']['data']
      cursor_json = json['result']['cursor']

      @results = []

      users_json.each { |u|
        phid = u['phid']
        username = u['fields']['username']
        realname = u['fields']['realName']
        roles = u['fields']['roles']

        user = UserLazyModel.new(phid)
        user.manual_init(username, realname, nil, roles)

        @results.append(user)
      }

      @cursor = Cursor.new(cursor_json['limit'], cursor_json['after'],
                           cursor_json['before'], cursor_json['order'])
    end
  end
end
