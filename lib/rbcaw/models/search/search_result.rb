require_relative '../../models/search/cursor'

module RbCAW
  class SearchResult
    attr_reader :results, :cursor

    def to_s
      return "#{{results: results, cursor: cursor}}"
    end
  end
end
