require_relative '../../models/search/search_result'
require_relative '../../models/project_lazy_model'
require_relative '../../models/user_lazy_model'

module RbCAW
  class ProjectSearchResult < SearchResult
    def initialize(conduit, json)
      projects_json = json['result']['data']
      cursor_json = json['result']['cursor']

      @results = []

      projects_json.each { |p|
        id = p['id']
        phid = p['phid']
        name = p['fields']['name']
        description = p['fields']['description']
        slug = p['fields']['slug']
        icon = p['fields']['icon']['key']
        color = p['fields']['color']['key']
        date_created = p['fields']['dateCreated']
        date_modified = p['fields']['dateModified']
        start_date = p['fields']['startdate']
        end_date = p['fields']['enddate']
        is_sprint = p['fields']['issprint']

        begin
          members = []

          for member_json in p['attachments']['members']['members']
            member = UserLazyModel.new(member_json['phid'], conduit: conduit)
            members.append(member)
          end
        rescue NoMethodError
          members = []
        end

        project = ProjectLazyModel.new(phid)
        project.manual_init(
            id, name, description, icon, color, members, slug,
            date_created, date_modified, start_date, end_date, is_sprint
        )

        @results.append(project)
      }

      @cursor = Cursor.new(cursor_json['limit'], cursor_json['after'],
                           cursor_json['before'], cursor_json['order'])
    end
  end
end
