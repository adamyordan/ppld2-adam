module RbCAW
  class BaseConduitFetcher
    attr_reader :conduit
    def initialize(conduit)
      @conduit = conduit
    end

    def search(*)
      raise NoMethodError, 'Method search not defined'
    end

    def get(phid:)
      if phid == nil
        return nil
      end

      search_result = search(constraints: {'phids' => [phid]}, limit: 1)

      if search_result.results.length == 0
        return nil
      end

      return search_result.results[0]
    end

    protected
    def conduit_search(method: nil, query_key: 'all', constraints: {}, attachments: {},
                       order: nil, before: nil, after: nil, limit: 100)
      if method == nil
        raise ArgumentError, 'Conduit method name cannot be null'
      end

      params = {
          'queryKey' => query_key,
          'constraints' => constraints,
          'attachments' => attachments,
          'order' => order,
          'before' => before,
          'after' => after,
          'limit' => limit
      }

      return @conduit.request(method, params: params)
    end
  end
end