require_relative '../fetchers/base_conduit_fetcher'

module RbCAW
  class UtilConduitFetcher < BaseConduitFetcher
    def ping
      return @conduit.request('conduit.ping')
    end
  end
end
