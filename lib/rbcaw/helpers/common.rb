class Common
  def self.param_to_form(container, ids=[])
    result = {}
    keys = []

    if container.is_a? Hash
      keys = container.keys
    elsif container.is_a? Array
      keys = 0..(container.length-1)
    end

    keys.each { |k|
      if (container[k].is_a? Hash) || (container[k].is_a? Array)
        result.merge!(param_to_form(container[k], ids + [k.to_s]))
      elsif container[k] != nil
        result.merge!(element_to_param(container[k], ids + [k.to_s]))
      end
    }

    return result
  end

  def self.element_to_param(val, ids=[])
    param_name = ids[0]
    ids = ids[1..(ids.length-1)]

    return { get_arg_id(param_name, ids) => val }
  end

  def self.get_arg_id(param_name, ids)
    result = param_name

    ids.each { |id|
      result = result + "[#{id}]"
    }

    return result
  end
end