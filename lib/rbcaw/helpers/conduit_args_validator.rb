class ConduitArgsValidator
  @@CONDUIT_METHODS = {
      'project_search' => {
          'constraints' => {
              ids: Array,
              phids: Array,
              name: String,
              members: String,
              watchers: Array,
              isMilestone: Bool,
              icons: Array,
              colors: Array,
              parents: Array,
              ancestors: Array,
              issprint: Bool
          }
      }
  }

  def self.check_map(method, context, args)
    for arg in args.keys
      unless check(method, context, arg, args[arg].class)
        raise ArgumentError, "Argument #{arg} is missing or incorrect type"
      end
    end

    return true
  end

  def self.check(method, context, arg_name, arg_type)
    begin
      return @@CONDUIT_METHODS[method][context][arg_name] == arg_type
    rescue NoMethodError
      return false
    end
  end
end